resource "aws_alb" "this" {
  name            = "${var.service_name}-alb"
  subnets         = data.aws_subnet_ids.public.ids
  security_groups = [aws_security_group.alb.id]
}

resource "aws_alb_target_group" "frontend" {
  name        = "${var.service_name}-tg"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "ip"
}

resource "aws_alb_listener" "frontend" {
  load_balancer_arn = aws_alb.this.id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.frontend.id
    type             = "forward"
  }
}