data "aws_iam_policy" "execution_policy" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

data "aws_iam_policy_document" "ssm_assume" {
  version = "2012-10-17"
  statement {
    effect = "Allow"
    principals {
      identifiers = ["ecs-tasks.amazonaws.com"]
      type        = "Service"
    }
    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "this" {
  name                = "${var.service_name}-role"
  managed_policy_arns = [data.aws_iam_policy.execution_policy.arn]
  assume_role_policy  = data.aws_iam_policy_document.ssm_assume.json
}
