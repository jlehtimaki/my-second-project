data "aws_subnet_ids" "public" {
  vpc_id = var.vpc_id
  filter {
    name   = "tag:type"
    values = ["public"]
  }
}

data "aws_subnet_ids" "private" {
  vpc_id = var.vpc_id
  filter {
    name   = "tag:type"
    values = ["private"]
  }
}