resource "aws_s3_bucket" "this" {
  count  = 25
  bucket = "apprentice-${count.index}"
  acl    = "private"

  tags = {
    Name        = "apprentice-${count.index}"
  }
}
