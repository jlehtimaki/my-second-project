variable "service_name" {
  type    = string
  default = "apprentice-24"
}

variable "vpc_id" {
  type    = string
  default = "vpc-0a4667000b9714318"
}

variable "frontend_port" {
  type    = number
  default = 80
}

variable "tag" {
  type = string
  description = "Image tag to be used"
}
