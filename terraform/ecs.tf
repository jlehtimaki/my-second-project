resource "aws_ecs_cluster" "this" {
  name               = var.service_name
  capacity_providers = ["FARGATE"]
}


resource "aws_ecs_task_definition" "this" {
  family                   = var.service_name
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = "256"
  memory                   = "512"
  execution_role_arn       = aws_iam_role.this.arn
  container_definitions = jsonencode([
    {
      name      = "frontend"
      image     = "126895207637.dkr.ecr.eu-west-1.amazonaws.com/apprentice-24-frontend:${var.tag}"
      cpu       = 10
      memory    = 512
      essential = true
      portMappings = [
        {
          containerPort = 80
          hostPort      = 80
        }
      ]
    }
  ])
}

resource "aws_ecs_service" "this" {
  name            = var.service_name
  cluster         = aws_ecs_cluster.this.id
  task_definition = aws_ecs_task_definition.this.id
  desired_count   = 1
  launch_type     = "FARGATE"
  network_configuration {
    subnets         = data.aws_subnet_ids.private.ids
    security_groups = [aws_security_group.ecs_tasks.id]
  }
  load_balancer {
    target_group_arn = aws_alb_target_group.frontend.arn
    container_name   = "frontend"
    container_port   = 80
  }

  depends_on = [
    aws_alb_listener.frontend,
  ]
}

output "alb_address" {
  value = aws_alb.this.dns_name
}
