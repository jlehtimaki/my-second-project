FROM ubuntu:18.04
WORKDIR /opt/myapp
COPY app.py .
RUN apt-get update && \
    apt-get install -y python python-pip && \
    pip install --no-cache Flask && \
    apt-get remove -y python-pip && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/*
CMD ["python", "app.py"]
